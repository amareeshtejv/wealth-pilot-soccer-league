package com.wealthpilot.challenge.soccerleague.services;

import com.wealthpilot.challenge.soccerleague.domain.Schedule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest class PrepareScheduleImplTest
{
    @Autowired private PrepareSchedule prepareSchedule;


    @Test void test_prepareLeagueSchedule() throws IOException
    {
        PrepareScheduleImpl prepareScheduleImpl = mock(PrepareScheduleImpl.class);
        doNothing().when(prepareScheduleImpl).prepareLeagueSchedule();
        prepareScheduleImpl.prepareLeagueSchedule();
        verify(prepareScheduleImpl, times(1)).prepareLeagueSchedule();
    }


    @Test void test_printSchedule()
    {
        Schedule[] inSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build()};
        String leagueName = "Wealthpilot Soccer league";
        String country = "Germany";
        String scheduleType = "Simple";
        PrepareScheduleImpl prepareScheduleImpl = mock(PrepareScheduleImpl.class);
        doNothing().when(prepareScheduleImpl).printSchedule(inSchedule, leagueName, country, scheduleType);
        prepareScheduleImpl.printSchedule(inSchedule, leagueName, country, scheduleType);
        verify(prepareScheduleImpl, times(1)).printSchedule(inSchedule, leagueName, country, scheduleType);
    }


    @DisplayName("Simple - Test getting 2nd schedule") @Test void test_getSecondSchedule_simple()
    {
        Schedule[] outSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("19.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("26.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("16.04.2022").time("15:00").team1("Italy").team2("Germany").groupFlag(true).build(),
                 Schedule.builder().date("23.04.2022").time("15:00").team1("Poland").team2("France").groupFlag(true).build(),
                 Schedule.builder().date("30.04.2022").time("15:00").team1("Poland").team2("Germany").groupFlag(true).build(),
                 Schedule.builder().date("07.05.2022").time("15:00").team1("Italy").team2("France").groupFlag(true).build()};

        Schedule[] inSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("19.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("26.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build()};

        int noOfGroups = 0;
        assertEquals(Arrays.toString(outSchedule),
                     Arrays.toString(prepareSchedule.getSecondSchedule(inSchedule, noOfGroups)));
    }


    @DisplayName("Advanced - Test getting 2nd schedule") @Test void test_getSecondSchedule_advanced()
    {
        Schedule[] outSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("05.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("02.04.2022").time("15:00").team1("Italy").team2("Germany").groupFlag(true).build(),
                 Schedule.builder().date("02.04.2022").time("15:00").team1("Poland").team2("France").groupFlag(true).build(),
                 Schedule.builder().date("09.04.2022").time("15:00").team1("Poland").team2("Germany").groupFlag(true).build(),
                 Schedule.builder().date("09.04.2022").time("15:00").team1("Italy").team2("France").groupFlag(true).build()};

        Schedule[] inSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("05.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build(),
                 Schedule.builder().date("").time("").team1("").team2("").groupFlag(false).build()};

        int noOfGroups = 2;
        assertEquals(Arrays.toString(outSchedule),
                     Arrays.toString(prepareSchedule.getSecondSchedule(inSchedule, noOfGroups)));
    }


    @DisplayName("Advanced - Test getting round 1 schedule") @Test void test_getAdvancedSchedule()
    {
        Schedule[] outSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(true).build(),
                 Schedule.builder().date("05.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(true).build(),
                 Schedule.builder().date("12.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(true).build()};

        Schedule[] inSchedule =
                {Schedule.builder().date("05.03.2022").time("15:00").team1("Germany").team2("Italy").groupFlag(false).build(),
                 Schedule.builder().date("02.03.2022").time("15:00").team1("France").team2("Poland").groupFlag(false).build(),
                 Schedule.builder().date("09.03.2022").time("15:00").team1("Germany").team2("Poland").groupFlag(false).build(),
                 Schedule.builder().date("16.03.2022").time("15:00").team1("France").team2("Italy").groupFlag(false).build()};

        int noOfGroups = 2;
        LocalDate startDate = LocalDate.parse("2022-03-05");
        Set<String> soccerTeams = new HashSet<>();
        soccerTeams.add("Germany");
        soccerTeams.add("Italy");
        soccerTeams.add("Poland");
        soccerTeams.add("France");
        assertEquals(Arrays.toString(outSchedule),
                     Arrays.toString(prepareSchedule.getAdvancedSchedule(inSchedule,
                                                                         noOfGroups,
                                                                         startDate,
                                                                         soccerTeams)));
    }


    @DisplayName("Test calculating no of matches") @Test void test_calculateNoOfMatches()
    {
        int n = 6;
        int k = 2;
        assertEquals(30, prepareSchedule.calculateNoOfMatches(n, k));
    }


    @DisplayName("Negative scenario - Calculating no of matches") @Test void test_calculateNoOfMatches_negativeCase()
    {
        int n = 6;
        int k = 2;
        assertNotEquals(42, prepareSchedule.calculateNoOfMatches(n, k));
    }


    @DisplayName("Check factorial") @Test void test_getFactorial()
    {
        int num = 4;
        assertEquals(24, prepareSchedule.getFactorial(num));
    }


    @DisplayName("Check factorial - Negative case") @Test void test_getFactorial_negativeCase()
    {
        int num = 4;
        assertNotEquals(30, prepareSchedule.getFactorial(num));
    }


    @DisplayName("Case 1 - Calculate no of groups - Advanced") @Test void test_getLeagueGroupCount()
    {
        int noOfMatches = 15;
        int noOfTeams = 6;
        assertEquals(7, prepareSchedule.getLeagueGroupCount(noOfMatches, noOfTeams));
    }


    @DisplayName("Case 2 - Calculate no of groups - Advanced") @Test void test_getLeagueGroupCount_gtThanOne()
    {
        int noOfMatches = 28;
        int noOfTeams = 8;
        assertEquals(9, prepareSchedule.getLeagueGroupCount(noOfMatches, noOfTeams));
    }


    @DisplayName("Case 3 - Calculate no of groups - Advanced") @Test void test_getLeagueGroupCount_betweenZeroToOne()
    {
        int noOfMatches = 21;
        int noOfTeams = 7;
        assertEquals(7, prepareSchedule.getLeagueGroupCount(noOfMatches, noOfTeams));
    }

}