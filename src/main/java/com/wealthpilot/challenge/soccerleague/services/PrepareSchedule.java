package com.wealthpilot.challenge.soccerleague.services;

import com.wealthpilot.challenge.soccerleague.domain.Schedule;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Set;

public interface PrepareSchedule
{
    void prepareLeagueSchedule() throws IOException;

    Schedule[] getSecondSchedule(Schedule[] leagueSchedule, Integer noOfGroups);

    int calculateNoOfMatches(Integer n, Integer k);

    int getFactorial(Integer num);

    int getLeagueGroupCount(Integer noOfMatches, Integer noOfTeams);

    Schedule[] getAdvancedSchedule(Schedule[] leagueSchedule,
                                   Integer noOfGroups,
                                   LocalDate startDate,
                                   Set<String> soccerTeams);

    void printSchedule(Schedule[] finalLeagueSchedule, String leagueName, String country, String scheduleType);

}
