package com.wealthpilot.challenge.soccerleague.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wealthpilot.challenge.soccerleague.domain.Schedule;
import com.wealthpilot.challenge.soccerleague.domain.ScheduleType;
import com.wealthpilot.challenge.soccerleague.domain.SoccerLeague;
import org.apache.commons.math3.util.Combinations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Service public class PrepareScheduleImpl implements PrepareSchedule
{
    private static final String MATCH_TIME = "15:00";
    private static final String MATCH_DATE = "2022-03-05";
    private static final int TEAMS_PER_MATCH = 2;
    private static final int NO_OF_WEEKS_BETWEEN_MATCH = 1;
    private static final int NO_OF_WEEKS_BETWEEN_ROUNDS = 3;


    public static <T extends Serializable> T clone(T object)
    {
        return (T) SerializationUtils.deserialize(SerializationUtils.serialize(object));
    }


    @Override public void prepareLeagueSchedule() throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        File resource = new ClassPathResource("soccer_teams.json").getFile();
        SoccerLeague soccerLeague = objectMapper.readValue(resource, SoccerLeague.class);
        Set<String> soccerTeams = new HashSet<>();
        Arrays.stream(soccerLeague.getTeams()).forEach(team -> soccerTeams.add(team.getName()));
        String[] leagueTeams = soccerTeams.toArray(new String[0]);
        Combinations combination = new Combinations(leagueTeams.length, TEAMS_PER_MATCH);
        LocalDate leagueStartDate = LocalDate.parse(MATCH_DATE);
        LocalDate startDate = leagueStartDate;
        Schedule[] leagueSchedule = new Schedule[calculateNoOfMatches(leagueTeams.length, TEAMS_PER_MATCH)];
        int counter = 0;
        for(int[] selected : combination)
        {
            int teamCount = 0;
            String team1 = "", team2 = "";
            for(int nameIndex : selected)
            {
                if(teamCount == 0)
                {
                    team1 = leagueTeams[nameIndex];
                }
                else
                {
                    team2 = leagueTeams[nameIndex];
                }
                teamCount++;
            }
            leagueSchedule[counter] = new Schedule(leagueStartDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                                                   MATCH_TIME,
                                                   team1,
                                                   team2,
                                                   false);
            counter++;
            leagueStartDate = leagueStartDate.plusWeeks(NO_OF_WEEKS_BETWEEN_MATCH);
        }
        int noOfLeagueGroups = getLeagueGroupCount((leagueSchedule.length) / 2, leagueTeams.length);
        Schedule[] advancedLeagueSchedule =
                Arrays.copyOf(getAdvancedSchedule(leagueSchedule, noOfLeagueGroups, startDate, soccerTeams),
                              leagueSchedule.length);
        Schedule[] finalLeagueSchedule = getSecondSchedule(leagueSchedule, 0);
        Schedule[] finalAdvancedLeagueSchedule = getSecondSchedule(advancedLeagueSchedule, noOfLeagueGroups);
        printSchedule(finalLeagueSchedule,
                      soccerLeague.getLeague(),
                      soccerLeague.getCountry(),
                      String.valueOf(ScheduleType.Simple));
        printSchedule(finalAdvancedLeagueSchedule,
                      soccerLeague.getLeague(),
                      soccerLeague.getCountry(),
                      String.valueOf(ScheduleType.Advanced));
    }


    @Override public Schedule[] getSecondSchedule(Schedule[] leagueSchedule, Integer noOfGroups)
    {
        int nextSetCounter = (leagueSchedule.length) / 2;
        int tempCounter = nextSetCounter;
        for(Schedule team : leagueSchedule)
        {
            if(nextSetCounter < tempCounter * 2)
            {
                DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                LocalDate random = LocalDate.parse(team.getDate(), df);
                if(noOfGroups > 0)
                {
                    random = random.plusWeeks((noOfGroups - 1) + NO_OF_WEEKS_BETWEEN_ROUNDS);
                }
                else
                {
                    random = random.plusWeeks((tempCounter - 1) + NO_OF_WEEKS_BETWEEN_ROUNDS);
                }
                leagueSchedule[nextSetCounter] = new Schedule(random.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                                                              MATCH_TIME,
                                                              team.getTeam2(),
                                                              team.getTeam1(),
                                                              team.getGroupFlag());
                nextSetCounter++;
            }
        }
        return leagueSchedule;
    }


    @Override public int calculateNoOfMatches(Integer n, Integer k)
    {
        return (getFactorial(n) / (getFactorial(k) * getFactorial(n - k))) * 2;
    }


    @Override public int getFactorial(Integer num)
    {
        int i, fact = 1;
        for(i = 1; i <= num; i++)
            fact = fact * i;
        return fact;
    }


    @Override public int getLeagueGroupCount(Integer noOfMatches, Integer noOfTeams)
    {
        int noOfLeagueGroups;
        if((noOfMatches) % (noOfTeams) == 0)
        {
            noOfLeagueGroups = noOfTeams;
        }
        else if((noOfMatches) % (noOfTeams) > 1)
        {
            noOfLeagueGroups = noOfTeams + 1;
        }
        else
        {
            noOfLeagueGroups = noOfTeams - 1;
        }
        return noOfLeagueGroups;
    }


    @Override public Schedule[] getAdvancedSchedule(Schedule[] leagueSchedule,
                                                    Integer noOfGroups,
                                                    LocalDate startDate,
                                                    Set<String> soccerTeams)
    {
        ArrayList<ArrayList<Schedule>> advancedScheduleList = new ArrayList<>(noOfGroups);
        ArrayList<Schedule> scheduleList;
        Schedule[] tempLeagueSchedule = clone(leagueSchedule);
        Set<String> tempSet;
        for(int i = 0; i < noOfGroups; i++)
        {
            tempSet = new HashSet<>(soccerTeams);
            scheduleList = new ArrayList<>();
            for(Schedule schedule : tempLeagueSchedule)
            {
                if(schedule != null)
                {
                    if(!schedule.getGroupFlag())
                    {
                        if(tempSet.contains(schedule.getTeam1()) && tempSet.contains(schedule.getTeam2()))
                        {
                            schedule.setGroupFlag(true);
                            schedule.setDate(startDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                            scheduleList.add(schedule);
                            tempSet.remove(schedule.getTeam1());
                            tempSet.remove(schedule.getTeam2());
                        }
                    }
                    if(tempSet.size() == 0)
                    {
                        break;
                    }
                }
            }
            startDate = startDate.plusWeeks(NO_OF_WEEKS_BETWEEN_MATCH);
            advancedScheduleList.add(scheduleList);
        }
        return Stream.of(advancedScheduleList.stream().map(arr -> arr.toArray(Schedule[]::new)).toArray(Schedule[][]::new)).flatMap(
                Stream::of).toArray(Schedule[]::new);
    }


    @Override public void printSchedule(Schedule[] finalLeagueSchedule,
                                        String leagueName,
                                        String country,
                                        String scheduleType)
    {
        System.out.println(scheduleType + " Schedule:");
        System.out.println("League : " + leagueName + " in " + country);
        String format = "%-15s %10s %20s %25s\n";
        System.out.format(format, "Date", "Time", "Team 1", "Team 2");
        for(Schedule sc : finalLeagueSchedule)
        {
            System.out.format(format, sc.getDate(), sc.getTime(), sc.getTeam1(), sc.getTeam2());
        }
        System.out.println();
    }

}
