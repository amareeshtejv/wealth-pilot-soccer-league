package com.wealthpilot.challenge.soccerleague;

import com.wealthpilot.challenge.soccerleague.services.PrepareSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication public class SoccerLeagueApplication implements CommandLineRunner
{
    @Autowired private PrepareSchedule prepareSchedule;


    public static void main(String[] args)
    {
        SpringApplication.run(SoccerLeagueApplication.class, args);
    }


    @Override public void run(String[] args) throws IOException
    {
        prepareSchedule.prepareLeagueSchedule();
    }
}
