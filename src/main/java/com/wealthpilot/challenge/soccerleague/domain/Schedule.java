package com.wealthpilot.challenge.soccerleague.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Schedule implements Serializable
{
    private String date;
    private String time;
    private String team1;
    private String team2;
    private Boolean groupFlag;
}
