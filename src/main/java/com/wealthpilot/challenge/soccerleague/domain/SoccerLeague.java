package com.wealthpilot.challenge.soccerleague.domain;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SoccerLeague
{
    private String league;
    private String country;
    private Teams[] teams;
}
