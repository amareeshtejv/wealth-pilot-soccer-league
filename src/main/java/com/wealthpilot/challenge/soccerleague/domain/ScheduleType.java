package com.wealthpilot.challenge.soccerleague.domain;

public enum ScheduleType
{
    Simple, Advanced
}
