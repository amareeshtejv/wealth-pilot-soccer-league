package com.wealthpilot.challenge.soccerleague.domain;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Teams
{
    private String name;
    private String founding_date;
}
