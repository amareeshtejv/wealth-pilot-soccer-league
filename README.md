##Description
This application is to create Schedule for Soccer league based on the JSON file under resource folder and the date provided.

##External Libraries used
1) Apache Commons
2) lombok
3) jackson-databind

##Non-functional requirement
This project runs on Java 11 and Spring Boot.
Used Junit and Mockito for Unit test cases

##Build
To build the project, please run below command to create jar file.
`mvn clean install`

##Test cases
Added 12 Jnuit test cases to evaluate functionlities.

##Output - Screen shots
Added few application screen shots to application_screenshots folder.
It contains screenshots related to outputs, application startup, Junit test cases and successful build.
